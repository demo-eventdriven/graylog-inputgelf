FROM graylog2/graylog:2.4.3-1
WORKDIR /usr/share/graylog

COPY graylog_gelfinput_contentpack.json data/contentpacks/.

VOLUME /usr/share/graylog/data
EXPOSE 9000 12201 514
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["graylog"]